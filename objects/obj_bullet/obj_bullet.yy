{
    "id": "92d5c914-069b-4115-bd43-26bae337c68a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "6676cc8f-46e4-4a8b-bee6-48822f4e80c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "92d5c914-069b-4115-bd43-26bae337c68a"
        },
        {
            "id": "4b4105cc-5006-4672-9b5e-7bc76beccb7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "bc75e712-67ad-4e73-afef-76c16ab92e76",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "92d5c914-069b-4115-bd43-26bae337c68a"
        },
        {
            "id": "65480200-af56-469a-833f-956bfcfe7ed2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "92d5c914-069b-4115-bd43-26bae337c68a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "05f39ca9-f35e-449d-8222-4357e2762a23",
    "visible": true
}