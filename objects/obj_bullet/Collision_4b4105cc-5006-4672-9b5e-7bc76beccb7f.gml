/// @description ?
score +=10
instance_destroy();
with(other){

	
	repeat(10){
			var debris = instance_create_layer(x,y,"Roids", obj_debris)
			debris.direction = irandom_range(0,359);
			debris.speed += 0.7;
			debris.hspeed += hspeed;
			debris.vspeed += vspeed;
		}
	
	if(sprite_index == spr_Roid_L){
		audio_play_sound(roidsmashL,1,0)
		repeat(4)			{
			var new_asteroid = instance_create_layer(x,y,"Roids",obj_Roid);
			new_asteroid.sprite_index = spr_Roid_M;
			new_asteroid.direction = irandom_range(0,359);
			new_asteroid.speed =+ 0.3;
			new_asteroid.hspeed += hspeed;
			new_asteroid.vspeed += vspeed;
			}
	
	} else if(sprite_index == spr_Roid_M){
		audio_play_sound(roidsmashm,1,0)
		repeat(4)
				{
			var new_asteroid = instance_create_layer(x,y,"Roids",obj_Roid);
			new_asteroid.sprite_index = spr_Roid_S;
			new_asteroid.direction = irandom_range(0,359);
			new_asteroid.speed =+ 0.5;
			new_asteroid.hspeed += hspeed;
			new_asteroid.vspeed += vspeed;
		}
	}	
		//destroy bullet
	audio_play_sound(roidsmashs,1,0)
instance_destroy();	
}

