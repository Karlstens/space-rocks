{
    "id": "8345f7de-a2f4-4040-92cd-71d5846ffc6a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player1",
    "eventList": [
        {
            "id": "3b9d5a53-1784-459e-a2a2-fb497631d763",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8345f7de-a2f4-4040-92cd-71d5846ffc6a"
        },
        {
            "id": "22cc3cef-3a9a-45fe-afb9-7188d3b1ae71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8345f7de-a2f4-4040-92cd-71d5846ffc6a"
        },
        {
            "id": "e4d44686-7d5d-4d1e-8ec3-a7910fd08613",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "bc75e712-67ad-4e73-afef-76c16ab92e76",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8345f7de-a2f4-4040-92cd-71d5846ffc6a"
        },
        {
            "id": "6421dd93-6224-4fe5-bc7c-c469e26042a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8345f7de-a2f4-4040-92cd-71d5846ffc6a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aa37d77e-b78d-45b2-8bfe-61d9022698fe",
    "visible": true
}