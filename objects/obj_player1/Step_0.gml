/// @description ?
/// If left key, rotate ship counter clockwise. If right, rotate ship clockwise
/// If down key pressed, ship to halt - not reverse.

#region //left right controls
if keyboard_check(vk_left){
image_angle += 3
	var thrust = instance_create_layer((x+lengthdir_x(20,image_angle-135)),(y+lengthdir_y(20,image_angle-135)),"Roids", obj_gas)
			thrust.direction = image_angle - irandom_range(120,240);
			thrust.speed += 0.10;
			thrust.hspeed += hspeed;
			thrust.vspeed += vspeed;

}

if keyboard_check(vk_right){
image_angle -= 3
	var thrust = instance_create_layer((x+lengthdir_x(20,image_angle+135)),(y+lengthdir_y(20,image_angle+135)),"Roids", obj_gas)
			thrust.direction = image_angle - irandom_range(120,240);
			thrust.speed += 0.10;
			thrust.hspeed += hspeed;
			thrust.vspeed += vspeed;
}
#endregion

if keyboard_check(vk_up){
	motion_add(image_angle,(0.05));
	var thrust = instance_create_layer(x,y,"Roids", obj_gas);
		thrust.direction = image_angle - irandom_range(150,210);
		thrust.speed += 0.07;
		thrust.hspeed += hspeed;
		thrust.vspeed += vspeed;
			//scr_thrust(x,y);
}

if keyboard_check(vk_down){
	motion_add(image_angle,(-0.1));
	var thrust = instance_create_layer((x+lengthdir_x(20,image_angle-135)),(y+lengthdir_y(20,image_angle-135)),"Roids", obj_gas)
			thrust.direction = image_angle + irandom_range(-40,40);
			thrust.speed += 0.10;
			thrust.hspeed += hspeed;
			thrust.vspeed += vspeed;
	var thrust = instance_create_layer((x+lengthdir_x(20,image_angle+135)),(y+lengthdir_y(20,image_angle+135)),"Roids", obj_gas)
			thrust.direction = image_angle + irandom_range(-40,40);
			thrust.speed += 0.10;
			thrust.hspeed += hspeed;
			thrust.vspeed += vspeed;
}

move_wrap(1,1,16)

if keyboard_check_pressed(vk_space){
	var bullet =	instance_create_layer(x,y,"Player",obj_bullet);
	audio_play_sound(laser,1,0)
	bullet.direction = image_angle
	bullet.speed =+ 3
	bullet.hspeed += hspeed
	bullet.vspeed += vspeed
};

