{
    "id": "bd64c5b4-8d3a-4903-95f2-7b0e9802649f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_debris",
    "eventList": [
        {
            "id": "caeeef48-72df-4c88-9c1f-2ceade234c5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd64c5b4-8d3a-4903-95f2-7b0e9802649f"
        },
        {
            "id": "b24249d2-88d7-4e90-b827-a70e08851a46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd64c5b4-8d3a-4903-95f2-7b0e9802649f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73fa318d-394d-448c-8b99-b0f6f1cebbf3",
    "visible": true
}