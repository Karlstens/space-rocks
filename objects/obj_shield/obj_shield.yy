{
    "id": "2f6c9277-eccf-469b-b462-93b9bcbb09f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shield",
    "eventList": [
        {
            "id": "a0b0ae1d-cd1b-46fa-9235-d43b7f2e9463",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2f6c9277-eccf-469b-b462-93b9bcbb09f4"
        },
        {
            "id": "b8f5e11b-61e4-45b9-b1b5-005ef79a632d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f6c9277-eccf-469b-b462-93b9bcbb09f4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a701aab1-24d3-451e-b292-855e6b890235",
    "visible": true
}