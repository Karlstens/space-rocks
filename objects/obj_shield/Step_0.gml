/// @desc

//movement

if instance_exists(obj_player1) {
	x = obj_player1.x;
	y = obj_player1.y;
}


//shield energy

image_alpha = _Shield;

if !glow {
	_Shield -= 0.005;
}
else {
_Shield += 0.005;
}

if _Shield >= 1 or _Shield <= 0.5 {
 glow = !glow
}

