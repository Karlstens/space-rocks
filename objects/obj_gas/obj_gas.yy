{
    "id": "de2bd56a-560a-4d5d-915e-bf0d25a9048d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gas",
    "eventList": [
        {
            "id": "300ad533-1fbe-4481-91de-9b695cef2878",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de2bd56a-560a-4d5d-915e-bf0d25a9048d"
        },
        {
            "id": "4f4518b3-d74e-429c-8927-19296d1568df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "de2bd56a-560a-4d5d-915e-bf0d25a9048d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73fa318d-394d-448c-8b99-b0f6f1cebbf3",
    "visible": true
}