{
    "id": "bc75e712-67ad-4e73-afef-76c16ab92e76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Roid",
    "eventList": [
        {
            "id": "b3d54824-bab7-4ee2-a569-e6c5d9a6c477",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc75e712-67ad-4e73-afef-76c16ab92e76"
        },
        {
            "id": "b8b213ab-144b-43f9-9ce2-1ccd06650e67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bc75e712-67ad-4e73-afef-76c16ab92e76"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9231f18-453f-4fc2-ab13-d76ecfe12243",
    "visible": true
}