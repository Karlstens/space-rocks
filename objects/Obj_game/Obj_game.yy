{
    "id": "52e9e595-1d2b-436e-9d77-d8683a9bfb68",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game",
    "eventList": [
        {
            "id": "8f6bd00e-0353-48c7-9921-a1aec5a5c2a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52e9e595-1d2b-436e-9d77-d8683a9bfb68"
        },
        {
            "id": "1f9a4682-cbce-4f14-9093-807c4227fd99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "52e9e595-1d2b-436e-9d77-d8683a9bfb68"
        },
        {
            "id": "812f229c-db54-4c74-96ac-86e972f94925",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "52e9e595-1d2b-436e-9d77-d8683a9bfb68"
        },
        {
            "id": "ab04cf35-a157-4082-8a4c-b5f481dc9e6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "52e9e595-1d2b-436e-9d77-d8683a9bfb68"
        },
        {
            "id": "792a42cb-34e9-4136-88a9-14d71e6b9d9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "52e9e595-1d2b-436e-9d77-d8683a9bfb68"
        },
        {
            "id": "58bb1a69-1ca5-49fc-9714-6d213fba60d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "52e9e595-1d2b-436e-9d77-d8683a9bfb68"
        },
        {
            "id": "b9175f66-f946-43ba-b3f2-6c987cb98b39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "52e9e595-1d2b-436e-9d77-d8683a9bfb68"
        },
        {
            "id": "3f20040a-2737-47ef-8550-2ac21fe89bab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "52e9e595-1d2b-436e-9d77-d8683a9bfb68"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}