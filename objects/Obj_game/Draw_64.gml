/// @desc 

var c = c_yellow

switch(room){
	case rm_game:
		draw_set_halign(fa_left);
		if(instance_exists(obj_player1)){
			draw_text(100,20, "Score: "+string(score));
			draw_text(100,40, "Lives: "+string(lives));
		} else if (!instance_exists(obj_player1)){
					draw_text(100,60, "Press R to restart - still buggy");
		};
	break;
	
	case rm_start:
		draw_set_halign(fa_center);
		draw_text_transformed_color(
			room_width/2, 100, "SPACEROX",
			3,3,0, c,c,c,c, 1
		);
		
		draw_text(
			room_width/2, 200,
			@"Score 1,000 points to win!
UP:move
Left/Right: Change direction
Space: Shoot
>> PRESS ENTER TO START<<"
			);
	draw_set_halign(fa_center);
	break;
	
	case rm_win:
		draw_set_halign(fa_center);
		draw_text_transformed_color(
			room_width/2, 100, @"SPACEROX
- YOU WON -",
			3,3,0, c,c,c,c, 1
		);
	break;
	
	case rm_gameover:
		draw_set_halign(fa_center);
		draw_text_transformed_color(
			room_width/2, 100, @"SPACEROX 
- You dead! -",
			3,3,0, c,c,c,c, 1
		);
			draw_text_transformed_color(
			room_width/2, 200, "Score"+string(score),
			3,3,0, c,c,c,c, 1
		);
		
	break;
}