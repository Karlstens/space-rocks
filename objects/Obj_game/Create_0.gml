/// @desc Game Control Object

#macro RESOLUTION_W 512
#macro RESOLUTION_H 512

gifRecord = false;

score = 0;
lives = 3;

draw_set_font(fnt_text);

global.Game_stage = 1;