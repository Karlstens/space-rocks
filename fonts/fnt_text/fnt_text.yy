{
    "id": "eb06c369-56b8-4718-b8b5-64deab814544",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "eeb304f7-1153-485a-bcde-32786354ee95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "18956236-6ea7-4fbc-bef2-72da33a9cc77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 239,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "281d7472-d52f-4d6b-b315-9403c615ab9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 230,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "bf38af81-0152-497a-a57b-fb4c5408b2db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 213,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9c7e9ef6-d770-46bc-a843-36ae01f305ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 200,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fac02ef0-29b2-441d-b552-74634130ef10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 185,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b5ed78cd-16dd-4fe0-ab01-1df17904b3c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 172,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9cea5847-25f7-4120-850d-83fce1db4562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 167,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3a59b2f6-a1b3-4961-8093-7d8fd8ea093c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 159,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "636cd296-e324-4d0e-a025-bd0f8dbd6187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 151,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a3cda186-bb4b-4186-8c38-e4abb38e82bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 244,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "20e6d54e-7484-4bb6-9d9c-698a712bb188",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 141,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a35d5965-031d-4a2d-9344-6a49edbb5c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 123,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2b6c8c31-dfc8-4d01-9c28-36cd50dca56e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 113,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f059db47-5e2b-4a9b-bba1-0591c2d58fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 108,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f7600626-eaf7-4a85-b133-79469d351066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 98,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0d4694d0-220d-4e7e-805f-f1919e411a2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "bf9870af-2326-4570-a251-93fba1918731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 77,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ba080d9d-9217-4548-bca1-d6a6251d6db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 66,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ebcd6a09-a3bf-424b-a438-f1cab4ff69bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 55,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7e7da484-7328-488d-a127-0840412ee532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 43,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c0370b38-662a-48ac-857d-04db40fcff60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f9011319-e855-4f5d-a214-5a6effc26ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ea2bcc55-134b-4d4a-88ac-45703a93fce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "00f72711-d387-4d7a-bfd2-c1f641630590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ecc8e5a8-c6fc-4da4-9e3d-dbbf5bbf4514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 22,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "bd345b3f-6e94-4158-9120-b6e0f079ba4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 17,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "57fefb36-c6e6-4c2b-9fa3-f5e2fd8cd1a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 11,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "411c29c0-8b6c-4ac5-89ae-18c0fcb49cc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4977f12a-1d0d-49c9-a975-8378823f755a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 238,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "adcc00da-79ea-415d-a8eb-ec95272e6f89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 229,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "46e7d8af-c9e5-40f6-958c-688f593139d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "94a9e0bf-07a3-4cd5-9bed-e982b3e84ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 201,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b3ca3928-67dd-4777-96e1-b8abdaf9ac7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 188,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7305ec78-8c92-494f-ae78-d054196c02fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 177,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c9b0a64a-bff2-4efb-9a79-d054707f4c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 165,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3d39ce4e-b4ed-40a6-b240-770e73e7c303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 152,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "854fa5b4-5490-4e97-b9a9-3901393ccedd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 140,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4ffe72ad-0888-4e36-a593-b6430d14cb8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 128,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "19ae0127-9ad5-41be-ba62-635a078c4465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 115,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cbdd44ad-6d08-427b-8504-e2e6da5225f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 101,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4107fbd9-4878-430d-8af2-fc61333f01e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f6960f58-ba51-496d-83aa-b31b74273c35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 77,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "372b5ec8-c80a-46cd-8dee-d43697dfde1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 65,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3fe87ee2-a26b-4a37-a89c-4b79fa9fc6dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "aaa8d975-d963-4eda-affb-6d9c33ea8ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 38,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "15fefa65-bff8-4fe6-a838-02f59dffe36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 28,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e7118eb1-f1e5-4ae4-9586-6b560b59ed23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 13,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "077f658d-99cf-442e-8084-a89bd496aeb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "98dbed41-4812-4466-ba45-82c7617cc6fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 9,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cfe64bb0-a710-4a99-951e-5fda8b2c69ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f0a4256e-6b0d-433a-b68e-2d21d201f638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a9827972-36be-493f-acc5-3939e87709ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7b5aa8a6-e101-42e0-9a70-72bce110e6c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "67761551-0622-46d0-852f-4dd811ae6167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6ef2043e-a576-4e05-9f13-0cb8662997c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3de01007-2cc2-41e5-939c-8c5f55347072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f76929ce-2490-4a54-b8b6-b2408362c0ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "94798025-a34e-4565-befc-ed5af0398c6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8506f95d-8718-44e3-ae28-491f6474e154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9490b1f0-e42f-4413-89f3-8c15009c1709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "474b1104-4d71-48dc-a6eb-921818360ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "828f70f6-50c7-4c49-8dba-c0cc8d2d3ffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1d5a1d12-721e-48f9-a26c-de8453dbb12f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a5f81dec-60ab-4d3c-82af-688565610e65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "108d1556-a0a8-45e6-9914-cc356457ab83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0e677a05-5667-4a37-b3a4-c755ff2f92db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b0a2ba61-34ac-4a00-8d2b-2dacef1532e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9e12e98b-558c-4f6f-af24-ddd6fae4464c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7cce6b47-5759-4ca2-994b-b90fc051803b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8da2870d-efac-4d5a-bbb7-24642b871e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "bfa53ab1-156c-425d-9bae-7225edf1c830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 25,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e6d21bd8-8027-4c9d-84f3-b1612a8b5046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 131,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "90a59559-3ef2-4df2-9414-4535f708a9e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 36,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4a02a44a-0e97-453f-85bf-5db1b6d9b77b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 235,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "172be3c1-9e7c-4529-9ebd-612ef8e2e0df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 224,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "77353ade-dfce-42f8-a2fc-d3ce6d774737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 219,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "1ba14523-94b0-49cf-ae77-e0202482adf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 204,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6b2c5c52-cda2-4b1f-b56c-6109ee5a2d83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 193,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7b072dc0-eb38-419f-aae2-7c1e0f07768f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 183,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2ce870f7-239c-49e7-bebb-f928ed529cf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 172,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dfeff7f8-c2ea-4c85-8316-242490ef3af2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 162,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "aee24064-4c0b-4974-8938-d101723455cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 152,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "18954b90-09d1-46ba-9cff-c0b17fa56aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 244,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "59ed4f9e-9539-459b-819b-60140de3ae8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 142,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "116c2c18-3f87-47b1-a409-f8b720ac0072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 121,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c510d889-fa6f-4416-8930-72257d426df4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 111,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8b0109e0-9f19-46a7-af0b-da2fa577062e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 98,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "afd8de80-70de-4c52-b03a-ddc52f55dd31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8b5ce42b-0299-46a6-8047-bd9690c09164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 75,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2ea05056-2ef6-4fd3-8e7c-3de69da73417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 64,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "51622186-1539-48d1-a29e-05c6aafb2659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 55,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4b4e1a07-241f-4338-9310-ed53e3f14623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 50,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a7aac418-9061-4bed-ab6d-5780220b4982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 41,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "573b72d8-c2ca-49f5-81a7-12e91c76ea46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 34,
                "y": 98
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "f9db5f8e-cc42-4eb5-b6b4-8344a81c35f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 46,
                "y": 98
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}