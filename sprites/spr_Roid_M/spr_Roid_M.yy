{
    "id": "c9231f18-453f-4fc2-ab13-d76ecfe12243",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Roid_M",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f66680a9-896d-404d-b9d7-e056bcf2cf04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9231f18-453f-4fc2-ab13-d76ecfe12243",
            "compositeImage": {
                "id": "56a3ccf5-fd35-4cd9-874a-9d5c296f9726",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f66680a9-896d-404d-b9d7-e056bcf2cf04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "121efdd0-e0aa-40f8-82f7-386f998d3940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f66680a9-896d-404d-b9d7-e056bcf2cf04",
                    "LayerId": "5a44435e-7107-4247-b72d-a5d114297b49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5a44435e-7107-4247-b72d-a5d114297b49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9231f18-453f-4fc2-ab13-d76ecfe12243",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}