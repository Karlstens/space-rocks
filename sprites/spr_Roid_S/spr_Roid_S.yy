{
    "id": "3f7e6240-ed0c-4c08-aed2-1cc2f4fc3963",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Roid_S",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c85f1b8-9432-4a76-bcd4-09d3395aa408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f7e6240-ed0c-4c08-aed2-1cc2f4fc3963",
            "compositeImage": {
                "id": "fc93af52-9862-4e04-9d6e-3ec06739f757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c85f1b8-9432-4a76-bcd4-09d3395aa408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "add7226a-08d7-4186-a545-da7950ee644f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c85f1b8-9432-4a76-bcd4-09d3395aa408",
                    "LayerId": "edff432f-db56-49f8-bb76-f3713d7c3a67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "edff432f-db56-49f8-bb76-f3713d7c3a67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f7e6240-ed0c-4c08-aed2-1cc2f4fc3963",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}