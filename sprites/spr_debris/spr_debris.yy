{
    "id": "73fa318d-394d-448c-8b99-b0f6f1cebbf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dca9cd32-94dd-4529-97e7-7a6f1887887b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73fa318d-394d-448c-8b99-b0f6f1cebbf3",
            "compositeImage": {
                "id": "9673424b-0eab-42e7-ae12-3645a4ba9c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dca9cd32-94dd-4529-97e7-7a6f1887887b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6352584-93d5-4fb0-8b73-b69e03c76f30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca9cd32-94dd-4529-97e7-7a6f1887887b",
                    "LayerId": "deded1b5-f87f-417f-84ec-3e01699a7cab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "deded1b5-f87f-417f-84ec-3e01699a7cab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73fa318d-394d-448c-8b99-b0f6f1cebbf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 1,
    "yorig": 1
}