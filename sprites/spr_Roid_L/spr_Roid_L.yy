{
    "id": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Roid_L",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98fc0256-0b0b-4752-91e6-226a05c04211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
            "compositeImage": {
                "id": "ba4c46c3-78d9-4749-9bd2-6dc40f2ce3ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98fc0256-0b0b-4752-91e6-226a05c04211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d653f3c-2d31-451a-af52-86e57808c92a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98fc0256-0b0b-4752-91e6-226a05c04211",
                    "LayerId": "ff771916-b252-470d-8e91-65abc40d4e17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff771916-b252-470d-8e91-65abc40d4e17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}