{
    "id": "a701aab1-24d3-451e-b292-855e6b890235",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fccb17e-b871-4af1-a869-863704a52bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a701aab1-24d3-451e-b292-855e6b890235",
            "compositeImage": {
                "id": "cb701b96-94a2-45e9-91f9-cf21ac228302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fccb17e-b871-4af1-a869-863704a52bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998dace9-12f5-4010-9b89-6788d9051812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fccb17e-b871-4af1-a869-863704a52bb7",
                    "LayerId": "faf5e3ce-37cf-4b42-b82c-e95a2a1e6fc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "faf5e3ce-37cf-4b42-b82c-e95a2a1e6fc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a701aab1-24d3-451e-b292-855e6b890235",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 22
}